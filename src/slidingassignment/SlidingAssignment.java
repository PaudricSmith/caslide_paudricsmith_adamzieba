package slidingassignment;

import java.text.DecimalFormat;
import java.util.Scanner;

public class SlidingAssignment
{

    public static void main(String[] args)
    {
        // Initialising variables //////////////////////////////////////////////////////////////////
        // Eulers method ///////////////////////////////////////////////////////////////////////////

        double t;
        double tEnd;
        double h;
        double steps;
        int count = 0;

        // Position vector //
        double[] pBar =
        {
            0, 0, 0
        };

        // Velocity vector //
        double[] vBar =
        {
            0, 0, 0
        };

        ////////////////////////////////////////////////////////////////////////////////////////////
        // Force of gravity variables //////////////////////////////////////////////////////////////
        double mass;

        final double GRAVITY = 9.81;

        final double[] kHat =
        {
            0, 0, -1
        };

        double[] FgBar =
        {
            0, 0, 0
        };

        double[] FgnBar =
        {
            0, 0, 0
        };

        double[] FgpBar =
        {
            0, 0, 0
        };

        double FgpLength;
        ////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////
        // Normal variables ////////////////////////////////////////////////////////////////////////
        double[] nBar =
        {
            0, 0, 0
        };

        double[] nHat =
        {
            0, 0, 0
        };

        double nLength;

        // Force Normal variables //////////////////////////////////////////////////////////////////
        double[] FnBar =
        {
            0, 0, 0
        };

        double FnLength;
        ////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////
        // Friction variables //////////////////////////////////////////////////////////////////////
        double[] FfBar =
        {
            0, 0, 0
        };

        double FfLength = 0;

        double[] FfHat =
        {
            0, 0, 0
        };

        ////////////////////////////////////////////////////////////////////////////////////////////
        double[] dHat =
        {
            0, 0, 0
        };

        double[] FnetBar =
        {
            0, 0, 0
        };

        double[] aBar =
        {
            0, 0, 0
        };
        ////////////////////////////////////////////////////////////////////////////////////////////

        // Scanner to take in user inputs //////////////////////////////////////////////////////////
        Scanner input = new Scanner(System.in);
        DecimalFormat df = new DecimalFormat("0.0####");

        // Draw batman ascii logo //////////////////////////////////////////////////////////////////
        drawAppLogo();

        // Get input for variables from user ///////////////////////////////////////////////////////
        // Get the Position Vector from the user //
        System.out.println("Enter the Position vector: ");
        System.out.print("Position X = ");
        pBar[0] = input.nextDouble();
        System.out.print("Position Y = ");
        pBar[1] = input.nextDouble();
        System.out.print("Position Z = ");
        pBar[2] = input.nextDouble();

        // Get the Velocity Vector from the user //
        System.out.println("Enter the Velocity vector: ");
        System.out.print("Velocity X = ");
        vBar[0] = input.nextDouble();
        System.out.print("Velocity Y = ");
        vBar[1] = input.nextDouble();
        System.out.print("Velocity Z = ");
        vBar[2] = input.nextDouble();

        System.out.println("Enter the starting time: ");
        t = input.nextDouble();
        System.out.println("Enter the end time: ");
        tEnd = input.nextDouble();
        System.out.println("Enter h: ");
        h = input.nextDouble();

        steps = (tEnd - t) / h;
        System.out.println("Motion will be completed in " + (int) steps + " steps.");

        // Get main variables from user //
        System.out.print("An object of mass: ");
        mass = input.nextDouble();
        System.out.print("has been held at rest has just been released on a plane with \nNormal nBar X = ");
        nBar[0] = input.nextDouble();
        System.out.print("Normal nBar Y = ");
        nBar[1] = input.nextDouble();
        System.out.print("Normal nBar Z = ");
        nBar[2] = input.nextDouble();
        System.out.print("Enter Mew static: ");
        double mewStatic = input.nextDouble();
        System.out.print("Enter Mew kinetic: ");
        double mewKinetic = input.nextDouble();
        System.out.println("");

        // User must enter value smaller than mew static or will keep beening asked to enter ///////
        while (mewKinetic >= mewStatic)
        {
            System.out.println("\nMew kinetic must always be smaller than Mew static.");
            System.out.print("Enter Mew kinetic: ");
            mewKinetic = input.nextDouble();
        }

        // force of gravity calculations ///////////////////////////////////////////////////////////
        FgBar[0] = (mass * GRAVITY) * kHat[0];
        FgBar[1] = (mass * GRAVITY) * kHat[1];
        FgBar[2] = (mass * GRAVITY) * kHat[2];
        ////////////////////////////////////////////////////////////////////////////////////////////

        // CALCULATIONS ////////////////////////////////////////////////////////////////////////////
        // calculate length of Normal //////////////////////////////////////////////////////////////
        nLength = Math.sqrt(Math.pow(nBar[0], 2) + Math.pow(nBar[1], 2) + Math.pow(nBar[2], 2));

        // calculate direction of Normal ///////////////////////////////////////////////////////////
        nHat[0] = (1 / nLength) * nBar[0];
        nHat[1] = (1 / nLength) * nBar[1];
        nHat[2] = (1 / nLength) * nBar[2];

        // calculate force gravity normal //////////////////////////////////////////////////////////
        FgnBar[0] = (FgBar[2] * nHat[2]) * nHat[0];
        FgnBar[1] = (FgBar[2] * nHat[2]) * nHat[1];
        FgnBar[2] = (FgBar[2] * nHat[2]) * nHat[2];

        // calculate force gravity plane ///////////////////////////////////////////////////////////
        FgpBar[0] = FgBar[0] - FgnBar[0];
        FgpBar[1] = FgBar[1] - FgnBar[1];
        FgpBar[2] = FgBar[2] - FgnBar[2];

        // calculate length of force gravity plane//////////////////////////////////////////////////
        FgpLength = Math.sqrt(Math.pow(FgpBar[0], 2) + Math.pow(FgpBar[1], 2) + Math.pow(FgpBar[2], 2));

        // calculate force gravity plane //////////////////////////////////////////////////////////
        FnBar[0] = -1 * FgnBar[0];
        FnBar[1] = -1 * FgnBar[1];
        FnBar[2] = -1 * FgnBar[2];

        // calculate length of force gravity plane//////////////////////////////////////////////////
        FnLength = Math.sqrt(Math.pow(FnBar[0], 2) + Math.pow(FnBar[1], 2) + Math.pow(FnBar[2], 2));
        System.out.println("Fn length: " + FnLength);

        // calculate downhill direction ////////////////////////////////////////////////////////////
        dHat[0] = (1 / FgpLength) * FgpBar[0];
        dHat[1] = (1 / FgpLength) * FgpBar[1];
        dHat[2] = (1 / FgpLength) * FgpBar[2];

        // Calculate FfHat /////////////////////////////////////////////////////////////////////////
        FfHat[0] = -1 * dHat[0];
        FfHat[1] = -1 * dHat[1];
        FfHat[2] = -1 * dHat[2];

        // Calculate FfLength for static ///////////////////////////////////////////////////////////
        FfLength = mewStatic * FnLength;

        // Check if FgpLength against FfLength /////////////////////////////////////////////////////
        // if FgpLength is greater than FfLength = Kinetic /////////////////////////////////////////
        if (FgpLength > FfLength)
        {

            System.out.println("\nFgpLength > FfLength so object has kinetic energy...");
            // Calculate FfLength for kinetic //////////////////////////////////////////////////////
            FfLength = mewKinetic * FnLength;

            // Calculate FfBar /////////////////////////////////////////////////////////////////////
            FfBar[0] = (FfLength * FfHat[0]);
            FfBar[1] = (FfLength * FfHat[1]);
            FfBar[2] = (FfLength * FfHat[2]);

            // Calculate FnetBar ///////////////////////////////////////////////////////////////////
            FnetBar[0] = (FgpLength - FfLength) * dHat[0];
            FnetBar[1] = (FgpLength - FfLength) * dHat[1];
            FnetBar[2] = (FgpLength - FfLength) * dHat[2];

            // Calculate acceleration //////////////////////////////////////////////////////////////
            aBar[0] = (1 / mass) * FnetBar[0];
            aBar[1] = (1 / mass) * FnetBar[1];
            aBar[2] = (1 / mass) * FnetBar[2];

            System.out.println("\n*** Results ***");
            System.out.println("Step " + count);

            System.out.println("t" + count + " = " + t);
            System.out.println("p" + count + " = {" + df.format(pBar[0]) + ", " + df.format(pBar[1]) + ", "
                    + df.format(pBar[2]) + "}");
            System.out.println("v" + count + " = {" + df.format(vBar[0]) + ", " + df.format(vBar[1])
                    + ", " + df.format(vBar[2]) + "}");
            System.out.println("a" + count + " = {" + df.format(aBar[0]) + ", " + df.format(aBar[1]) + ", "
                    + df.format(aBar[2]) + "}");

            // loop through steps //
            for (int i = 0; i < steps; i++)
            {

                count++;
                System.out.println("Step " + count);

                // Calculate new t //
                t = t + h;

                // Calculate new Position //
                pBar[0] = pBar[0] + (h * (vBar[0]));
                pBar[1] = pBar[1] + (h * (vBar[1]));
                pBar[2] = pBar[2] + (h * (vBar[2]));

                // Calculate new Velocity //
                vBar[0] = vBar[0] + (h * (aBar[0]));
                vBar[1] = vBar[1] + (h * (aBar[1]));
                vBar[2] = vBar[2] + (h * (aBar[2]));

                // Calculate acceleration //
                aBar[0] = (1 / mass) * FnetBar[0];
                aBar[1] = (1 / mass) * FnetBar[1];
                aBar[2] = (1 / mass) * FnetBar[2];

                System.out.println("t" + count + " = " + t);
                System.out.println("p" + count + " = {" + df.format(pBar[0]) + ", " + df.format(pBar[1]) + ", "
                        + df.format(pBar[2]) + "}");
                System.out.println("v" + count + " = {" + df.format(vBar[0]) + ", " + df.format(vBar[1])
                        + ", " + df.format(vBar[2]) + "}");
                System.out.println("a" + count + " = {" + df.format(aBar[0]) + ", " + df.format(aBar[1]) + ", "
                        + df.format(aBar[2]) + "}");

                
            }
        } else // if FgpLength is less than or equal to FfLength = static //////////////////////////
        {
            System.out.println("\nObject is STATIC and not moving at this time, there for at rest.");
            System.out.println("FnetBar = 0");
            System.out.println("aBar = 0");
        }

    }

    public static void drawAppLogo()
    {
        System.out.println("========================================================================================");
        System.out.println("========================================================================================");
        System.out.println("===        #     #                                                    #     #        ===");
        System.out.println("===      ##       ##                                                ##       ##      ===");
        System.out.println("===    ###   # #   ###                                            ###   # #   ###    ===");
        System.out.println("===   ###    ###    ###                                          ###    ###    ###   ===");
        System.out.println("===  #####  #####  #####                                        #####  #####  #####  ===");
        System.out.println("=== #####################      Motion of object down hill      ##################### ===");
        System.out.println("=== #####################            using Friction            ##################### ===");
        System.out.println("===  ###################                                        ###################  ===");
        System.out.println("===   #### # ### # ####                                          #### # ### # ####   ===");
        System.out.println("===    ###    #    ###                                            ###    #    ###    ===");
        System.out.println("===      ##       ##                                                ##       ##      ===");
        System.out.println("===        #     #                                                    #     #        ===");
        System.out.println("========================================================================================");
        System.out.println("========================================================================================");
    }

}
